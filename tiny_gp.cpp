#include <iostream>
#include <string>
#include <cstdlib>
#include <chrono>
#include <limits>
#include <random>
#include <fstream>
#include <cmath>

using namespace std;

class tiny_gp {
	double * fitness;
	unsigned long seed;
	const static int POPSIZE = 100000;
	const static int DEPTH = 5;
	const static int MAX_LEN = 10000;
	double minrandom;
	double maxrandom;
	int varnumber;
	int randomnumber;
	int fitnesscases;
	double ** targets;
	std::default_random_engine * generator;
	const static int ADD = 110;
	const static int SUB = 111;
	const static int MUL = 112;
	const static int DIV = 113;
	const static int FSET_START = ADD;
	const static int FSET_END = DIV;
	double * x;
	char ** pop;
	char * buffer;
	char * program;
	int PC;
	constexpr static double CROSSOVER_PROB = 0.9;
	constexpr static double PMUT_PER_NODE  = 0.05;
	const static int GENERATIONS = 100;
	const static int TSIZE = 2;
	double fbestpop;
	double favgpop;
	double avg_len;
	public:
		tiny_gp (string, long);
		void evolve ();
		double nextDouble();
		void setup_fitness(string fname);
		char ** create_random_pop(int n, int depth, double * fitness);
		char * create_random_indiv(int depth);
		int grow(char * buffer, int pos, int max, int depth);
		int nextInt(int bound);
		double fitness_function(char * prog);
		int traverse(char * buffer, int buffercount);
		double run();
		void print_parms();
		void stats(double * fitness, char ** pop, int gen);
		int print_indiv(char * buffer, int buffercounter);
		int tournament(double * fitness, int tsize);
		char * crossover(char * parent1, char * parent2);
		char * mutation(char * parent, double pmut);
		int negative_tournament(double * fitness, int tsize);
		void arraycopy(char * source, int sourceIndex, char * destination, int destinationIndex, int length);
};

tiny_gp::tiny_gp (string fname, long s) {
	x = new double[FSET_START];
	buffer = new char[MAX_LEN];
	double fbestpop = 0.0;
        double favgpop = 0.0;
	cout.precision(15);

	fitness = new double[POPSIZE];
	if ( s >= 0 ) {
		seed = s;
	}
	else {
		seed = std::chrono::system_clock::now().time_since_epoch().count();
	}
	generator = new std::default_random_engine(seed);
	srand(seed);

	setup_fitness(fname);

	// Generating random constants
	for ( int i = 0; i < FSET_START; i ++ )
		x[i]= (maxrandom-minrandom) * (this->nextDouble()) + minrandom;

	pop = create_random_pop(POPSIZE, DEPTH, fitness);
}

void tiny_gp::print_parms() {
	cout << "-- TINY GP (C++ version) --\n";
	cout << "SEED="<<seed<<"\nMAX_LEN="<<MAX_LEN<<
			"\nPOPSIZE="<<POPSIZE<<"\nDEPTH="<<DEPTH<<
			"\nCROSSOVER_PROB="<<CROSSOVER_PROB<<
			"\nPMUT_PER_NODE="<<PMUT_PER_NODE<<
			"\nMIN_RANDOM="<<minrandom<<
			"\nMAX_RANDOM="<<maxrandom<<
			"\nGENERATIONS="<<GENERATIONS<<
			"\nTSIZE="<<TSIZE<<
			"\n----------------------------------\n";	
}

int tiny_gp::print_indiv(char * buffer, int buffercounter) {
	int a1=0, a2;
	if (buffer[buffercounter] < FSET_START) {
		if ( buffer[buffercounter] < varnumber )
			cout << "X" << (buffer[buffercounter] + 1 ) << " ";
		else
			cout << x[buffer[buffercounter]];
		return(++buffercounter);
	}
	switch(buffer[buffercounter]) {
		case ADD: cout << "(";
			  a1=print_indiv( buffer, ++buffercounter ); 
			  cout << " + "; 
			  break;
		case SUB: cout << "(";
			  a1=print_indiv( buffer, ++buffercounter ); 
			  cout << " - "; 
			  break;
		case MUL: cout << "(";
			  a1=print_indiv( buffer, ++buffercounter ); 
			  cout << " * "; 
			  break;
		case DIV: cout << "(";
			  a1=print_indiv( buffer, ++buffercounter ); 
			  cout << " / "; 
			  break;
	}
	a2=print_indiv( buffer, a1 ); 
	cout << ")"; 
	return( a2);
}

void tiny_gp::stats(double * fitness, char ** pop, int gen) {
	int best = this->nextInt(POPSIZE);
	int node_count = 0;
	fbestpop = fitness[best];
	favgpop = 0.0;

	for (int i = 0; i < POPSIZE; i++) {
		node_count += traverse(pop[i], 0);
		favgpop += fitness[i];
		if (fitness[i] > fbestpop) {
			best = i;
			fbestpop = fitness[i];
		}
	}
	avg_len = (double) node_count / POPSIZE;
	favgpop /= POPSIZE;
	cout << "Generation="<<gen<<" Avg Fitness="<<(-favgpop)<<
			" Best Fitness="<<(-fbestpop)<<" Avg Size="<<avg_len<<
			"\nBest Individual: ";
	print_indiv(pop[best], 0);

	cout << "\n";
}

void tiny_gp::arraycopy(char * source, int sourceIndex, char * destination, int destinationIndex, int length) {
	for(int i=0; i<length; i++) {
		destination[i+destinationIndex] = source[i+sourceIndex];
	}				
}

char * tiny_gp::crossover(char * parent1, char * parent2) {
	int xo1start, xo1end, xo2start, xo2end;
	char * offspring;
	int len1 = traverse(parent1, 0);
	int len2 = traverse(parent2, 0);
	int lenoff;

	xo1start =  this->nextInt(len1);
	xo1end = traverse(parent1, xo1start);

	xo2start =  this->nextInt(len2);
	xo2end = traverse(parent2, xo2start);

	lenoff = xo1start + (xo2end - xo2start) + (len1 - xo1end);

	offspring = new char[lenoff];

	this->arraycopy(parent1, 0, offspring, 0, xo1start);
	this->arraycopy(parent2, xo2start, offspring, xo1start,  
			(xo2end - xo2start));
	this->arraycopy(parent1, xo1end, offspring, 
			xo1start + (xo2end - xo2start), 
			(len1-xo1end));

	return offspring;
}

char * tiny_gp::mutation(char * parent, double pmut) {
	int len = traverse(parent, 0), i;
	int mutsite;
	char * parentcopy = new char [len];

	this->arraycopy(parent, 0, parentcopy, 0, len);
	for (i = 0; i < len; i ++ ) {  
		if ( this->nextDouble() < pmut ) {
			mutsite =  i;
			if ( parentcopy[mutsite] < FSET_START )
				parentcopy[mutsite] = (char) this->nextInt(varnumber+randomnumber);
			else
				switch(parentcopy[mutsite]) {
					case ADD: 
					case SUB: 
					case MUL: 
					case DIV:
						parentcopy[mutsite] = 
							(char) (this->nextInt(FSET_END - FSET_START + 1) 
									+ FSET_START);
				}
		}
	}

	return parentcopy;
}

int tiny_gp::negative_tournament(double * fitness, int tsize) {
	int worst = this->nextInt(POPSIZE), competitor;
	double fworst = 1e34;

	for (int i = 0; i < tsize; i++) {
		competitor = this->nextInt(POPSIZE);
		if ( fitness[competitor] < fworst ) {
			fworst = fitness[competitor];
			worst = competitor;
		}
	}
	return worst;
}

int tiny_gp::tournament(double * fitness, int tsize) {
	int best = this->nextInt(POPSIZE), i, competitor;
	double  fbest = -1.0e34;

	for (i = 0; i < tsize; i++) {
		competitor = this->nextInt(POPSIZE);
		if (fitness[competitor] > fbest) {
			fbest = fitness[competitor];
			best = competitor;
		}
	}
	return best;	
}

void tiny_gp::evolve () {
	int gen = 0, indivs, offspring, parent1, parent2, parent;
	double newfit;
	char * newind;
	print_parms();

	stats(fitness, pop, 0);
	for (gen = 1; gen < GENERATIONS; gen++) {
		if (fbestpop > -1e-5) {
			cout << "PROBLEM SOLVED\n";
			exit(0);
		}
		for (indivs = 0; indivs < POPSIZE; indivs++) {
			if (this->nextDouble() < CROSSOVER_PROB) {
				parent1 = tournament( fitness, TSIZE );
				parent2 = tournament( fitness, TSIZE );
				newind = crossover( pop[parent1],pop[parent2] );
			}
			else {
				parent = tournament( fitness, TSIZE );
				newind = mutation( pop[parent], PMUT_PER_NODE );
			}
			newfit = fitness_function( newind );
			offspring = negative_tournament( fitness, TSIZE );
			delete [] pop[offspring];
			pop[offspring] = newind;
			fitness[offspring] = newfit;
		}
		stats(fitness, pop, gen);
	}
	cout << "PROBLEM *NOT* SOLVED\n";
	exit(1);	
}

/* Interpreter */
double tiny_gp::run() {
	char primitive = program[PC++];
	if ( primitive < FSET_START )
		return x[primitive];
	switch (primitive) {
		case ADD : return( run() + run() );
		case SUB : return( run() - run() );
		case MUL : return( run() * run() );
		case DIV : { 
				   double num = run(), den = run();
				   if (std::abs(den) <= 0.001 ) 
					   return( num );
				   else 
					   return( num / den );
			   }
	}
	return 0.0; // It should never get here	
}

int tiny_gp::traverse(char * buffer, int buffercount) {
	if ( buffer[buffercount] < FSET_START )
		return ++buffercount;

	switch(buffer[buffercount]) {
		case ADD: 
		case SUB: 
		case MUL: 
		case DIV: 
			return(traverse(buffer, traverse(buffer, ++buffercount)));
	}
	return 0; // It should never get here		
}

double tiny_gp::fitness_function(char * prog) {
	int len;
	double result, fit = 0.0;
	
	len = traverse(prog, 0);

	for (int i = 0; i < fitnesscases; i ++ ) {
		for (int j = 0; j < varnumber; j ++ )
			x[j] = targets[i][j];
		program = prog;
		PC = 0;
		result = run();
		fit += std::abs(result - targets[i][varnumber]);
	}
	return -fit;
}

double tiny_gp::nextDouble() {
	double d = std::generate_canonical<double,std::numeric_limits<double>::digits>(*generator);
	return d;
}

int tiny_gp::nextInt(int bound) {
	return rand() % bound;
}

int tiny_gp::grow(char * buffer, int pos, int max, int depth) {
	char prim = (char) this->nextInt(2);

	int one_child;

	if (pos >= max) 
		return -1;

	if (pos == 0)
		prim = 1;

	if (prim == 0 || depth == 0) {
		prim = (char) this->nextInt(varnumber + randomnumber);
		buffer[pos] = prim;
		return pos+1;
	}
	else  {
		prim = (char) (this->nextInt(FSET_END - FSET_START + 1) + FSET_START);
		switch(prim) {
			case ADD: 
			case SUB: 
			case MUL: 
			case DIV:
				buffer[pos] = prim;
				one_child = grow(buffer, pos+1, max, depth-1);
				if (one_child < 0) 
					return -1;
				return grow(buffer, one_child, max,depth-1);
		}
	}
	return 0; // It should never get here
}

char * tiny_gp::create_random_indiv(int depth) {
	char * ind;
	int len;

	len = grow(buffer, 0, MAX_LEN, depth);

	ind = new char[len];

	this->arraycopy(buffer, 0, ind, 0, len);	
	
	return ind;
}

char ** tiny_gp::create_random_pop(int n, int depth, double * fitness) {
	char ** pop = new char * [n];

	for (int i = 0; i < n; i++) {
		pop[i] = create_random_indiv(depth);
		fitness[i] = fitness_function(pop[i]);
	}

	return pop;
}

void tiny_gp::setup_fitness(string fname) {
	ifstream file (fname);
	if(file.is_open()) {
		file >> varnumber;
		file >> randomnumber;
		file >> minrandom;
		file >> maxrandom;
		file >> fitnesscases;
	}
	else {
		cout << "Unable to open file '" << fname << "'" << endl;
		exit(1);
	}

	targets = new double * [fitnesscases];
	int columnsNumber = varnumber+1;
	for(int i=0; i<fitnesscases; i++) {
		targets[i] = new double[columnsNumber];
	}

	if (varnumber + randomnumber >= FSET_START) {
		cout << "Too many variables and constants";
		exit(2);
	}

	for (int i = 0; i < fitnesscases; i ++ ) {
		for (int j = 0; j <= varnumber; j++) {
			file >> targets[i][j];
		}
	}

	file.close();
}

int main (int argc, char* argv[])
{
	string fname = "problem.dat";
	long s = -1;

	if ( argc == 3 ) {
		s = atol(argv[1]);
		fname = argv[2];
	}
	if ( argc == 2 ) {
		fname = argv[1];
	}

	tiny_gp gp (fname, s);
	gp.evolve();

	return 0;
}


